function expandMenu(){
    document.querySelector('#nav-links').classList.toggle('expand');
}

class Course{
    constructor(title, courseid, credits, department, description){
        this.title = title;
        this.courseid = courseid;
        this.credits=credits;
        this.department=department;
        this.description=description;
    }
}

class Person{
    constructor(name){
        this.name = name;
    }
}

class Lecturer extends Person {
    constructor(name){
        super(name);
    } 
}



function load(){
    course = new Course("Introduction Project", "INFOB1PICA", "7.5 ECTS", "Computing Science", "komtnog");
    // lecturer = new lecturer("Ronnie Vanderfeesten");
    var div = document.getElementById('courseinfo');
    var myLink=document.createElement('h1');
    var myText=document.createTextNode(course.title);
    myLink.appendChild(myText);
    div.appendChild(myLink);
}

